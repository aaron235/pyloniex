# pyloniex

To initialize the API wrapper, call the following:

    import pyloniex

    pyloniexAPI = pyloniex.api( publicKey, privateKey, persistence=3, delay=5, verbose=True )

where `publicKey` is your public key, `privateKey` is your private key, `persistence` is the maximum number of requests the wrapper will make before accepting failure, `delay` is the wait period between each successive request, and `verbose` decides if the API wrapper will print whatever it is currently doing.

Currency pairs are always in the format `XXX_XXX`, as in:

* `BTC_LTC`
* `XMR_BTC`
* `XMR_QORA`

## Functions



### `pyloniexAPI.returnTicker()`

Returns:

    {
        'BTC_LTC': {
            'quoteVolume': '9286.30478348',
            'lowestAsk': '0.00787867',
            'last': '0.00780372',
            'isFrozen': '0',
            'low24hr': '0.00778020',
            'baseVolume': '72.81465031',
            'high24hr': '0.00794565',
            'highestBid': '0.00782350',
            'percentChange': '-0.00980209'
        },
        ...
    }

------

### `pyloniexAPI.return24hVolume()`

Returns:

    {
        'BTC_BITS': {
            'BTC': '0.01644340',
            'BITS': '1851.41759085'
        },
        'totalXMR': '8456.36711356',
        'BTC_NAV': {
            'NAV': '22214.23280753',
            'BTC': '0.11390919'
        },
        'BTC_CNMT': {
            'BTC': '0.09703426',
            'CNMT': '2167.65612768'
        },
        'BTC_NMC': {
            'BTC': '16.86993634',
            'NMC': '16413.74108590'
        }
        ...
    }

------

### `pyloniexAPI.returnOrderBook( currencyPair )`

------

### `pyloniexAPI.returnMarketTradeHistory( currencyPair )`

Accepts a currency pair.

Returns:

    [
        {
            'total': '0.00875138',
            'rate': '0.00788539',
            'globalTradeID': 13815060,
            'tradeID': 977067,
            'date': '2016-01-19 16:59:33',
            'type': 'buy',
            'amount': '1.10982209'
        },
        ...
    ]

------

### `pyloniexAPI.returnBalances()`

------

### `pyloniexAPI.returnCompleteBalances()`

------

### `pyloniexAPI.returnOpenOrders( currencyPair )`

------

### `pyloniexAPI.returnTradeHistory( currencyPair )`

------

### `pyloniexAPI.returnChartData( curencyPair, period, start, end )`

------

### `pyloniexAPI.buy( currencyPair, rate, amount )`

------

### `pyloniexAPI.sell( currencyPair, rate, amount )`

------

### `pyloniexAPI.cancel( currencyPair, orderNumber )`

------
