from urllib.parse import urlencode as urlencode
from urllib.request import urlopen as urlopen
from urllib.request import Request as Request
import urllib.error
import json
import time
import hmac
import hashlib


def createTimeStamp( datestr, format="%Y-%m-%d %H:%M:%S" ):
	return time.mktime( time.strptime( datestr, format ) )


class api:
	def __init__( self, publicKey, privateKey, persistence=3, delay=5, verbose=True ):
		"""
		Creates a Poloniex API object with the given APIKey and SecretAPIKey.
		Persistent HTTP requests are made with persistence and delay (set persistence to 1 for
		only one attempt). Delay is the time between re-requesting. The verbosity is on
		by default.
		"""
		self.APIKey = publicKey
		self.Secret = privateKey
		self.persistence = persistence
		self.delay = delay
		self.verbose = verbose

		# Define API paths:
		self.__PUBLIC_API_URL = "https://poloniex.com/public?command="
		self.__TRADING_API_URL = "https://poloniex.com/tradingApi"

	def __post_process( self, before ):
		after = before

		# Add timestamps if there isn't a timestamp, but there is a datetime
		if( 'return' in after ):
			if( isinstance( after['return'], list ) ):
				for x in xrange( 0, len( after['return'] ) ):
					if( isinstance( after['return'][x], dict ) ):
						if('datetime' in after['return'][x] and 'timestamp' not in after['return'][x]):
							after['return'][x]['timestamp'] = float(createTimeStamp(after['return'][x]['datetime']))

		return after

	def debugPrint( self, message ):
		"""
		Prints, but only if verbocity is set on.
		"""
		if ( self.verbose ):
			print( message )

	def persistentHTTPRequest( self, req ):
		"""
		Returns the JSON data of the given reqUrl, as a JSON-loaded dict. Tries a few times.
		"""
		for attempt in range( self.persistence ):
			try:
				res = urlopen( req )
			except urllib.error.HTTPError as e:
				self.debugPrint( e )
				self.debugPrint( "Retrying in " + str( self.delay ) + " seconds" )
				time.sleep( self.delay )
			else:
				return json.loads( res.read().decode( "utf-8" ) )

		return { 'error': 'HTTP request failed.' }

	def api_query( self, command, req={} ):
		"""
		Queries the API with arbitrary command and request paramaters. Returns
		an object formatted exactly as the API return.
		"""
		if ( command == "returnTicker" or command == "return24hVolume" ):
			params = command
			return self.persistentHTTPRequest( Request( self.__PUBLIC_API_URL + params ) )

		elif ( command == "returnOrderBook" ):
			params = command + '&currencyPair=' + str( req['currencyPair'] )
			return self.persistentHTTPRequest( self.__PUBLIC_API_URL + params )

		elif ( command == "returnMarketTradeHistory" ):
			params = "returnTradeHistory" + '&currencyPair=' + str( req['currencyPair'] )
			return self.persistentHTTPRequest( Request( self.__PUBLIC_API_URL + params ) )

		else:
			req['command'] = command
			req['nonce'] = int( time.time() * 1000 )
			postData = urlencode( req )

			postDataBytes = postData.encode( "utf-8" )
			secretBytes = self.Secret.encode( "utf-8" )

			sign = hmac.new( secretBytes, postDataBytes, hashlib.sha512 ).hexdigest()
			headers = {
				'Sign': sign,
				'Key': self.APIKey
				}

			req = Request( self.__TRADING_API_URL, postDataBytes, headers )
			return self.__post_process( self.persistentHTTPRequest( req ) )

	def returnTicker( self ):
		return self.api_query( "returnTicker" )

	def return24hVolume( self ):
		return self.api_query( "return24hVolume" )

	def returnOrderBook( self, currencyPair ):
		return self.api_query( "returnOrderBook", {'currencyPair': currencyPair} )

	def returnMarketTradeHistory( self, currencyPair ):
		return self.api_query( "returnMarketTradeHistory", {'currencyPair': currencyPair} )

	def returnBalances( self ):
		"""
		Returns all of your balances, as a dictionary, in the format:
		{
			'BTC': '1.23456789',
			'LTC': '0.12345678',
			...
		}
		"""
		return self.api_query( "returnBalances" )

	def returnCompleteBalances( self ):
		return self.api_query( "returnCompleteBalances" )

	def returnOpenOrders( self, currencyPair ):
		"""
		Returns all open orders you have for any given market as a currenty pair.
		Returns in the following format:
		[
			{
				'total': '0.00682632',
				'rate': '0.00000570',
				'date': '2016-01-19 15:49:04',
				'margin': 0,
				'type': 'sell',
				'amount': '1197.60000000',
				'orderNumber': '371763954
			}
			...
		]
		"""
		params = {
			"currencyPair": currencyPair
			}
		return self.api_query( "returnOpenOrders", params )

	def returnTradeHistory( self, currencyPair ):
		"""
		Returns your trade history for the market given in currencyPair.
		Returns in the format:
		[
			{
				'total': '0.00041000',
				'rate': '0.00000515',
				'globalTradeID': 18493390,
				'tradeID': '12380',
				'date': '2014-01-12 07:46:53',
				'fee': '0.00400000',
				'type': 'buy',
				'category': 'exchange',
				'amount': '400.00000000'
				'orderNumber': '372838969'
			},
			...
		]
		"""
		params = {
			"currencyPair": currencyPair
			}
		return self.api_query( 'returnTradeHistory', params )

	def returnChartData( self, currencyPair, period, start, end ):
		"""
		Returns a list of candlestick data on currencyPair, with intervals period,
		and starting at start and end.
		Returns in the format:
		[
			{
				"date": 1405699200,
				"high": 0.0045388,
				"low": 0.00403001,
				"open": 0.00404545,
				"close": 0.00427592,
				"volume": 44.11655644,
				"quoteVolume": 10259.29079097,
				"weightedAverage": 0.00430015
			},
			...
		]
		"""
		params = {
			"currencyPair": currencyPair,
			"period": period,
			"start": start,
			"end": end,
			}
		return self.api_query( 'returnChartData', params )

	# Places a buy order in a given market. Required POST parameters are "currencyPair",
	# "rate", and "amount". If successful, the method will return the order number.
	# Inputs:
	# currencyPair  The curreny pair
	# rate		  price the order is buying at
	# amount		Amount of coins to buy
	# Outputs:
	# orderNumber   The order number
	def buy( self, currencyPair, rate, amount ):
		"""
		Places an order to buy in any given market, with the given rate and amount.
		Returns data in the format:
		{
			'orderNumber': '372838969'
		}
		"""
		params = {
			"currencyPair": currencyPair,
			"rate": rate,
			"amount": amount
			}
		return self.api_query( 'buy', params )

	# Places a sell order in a given market. Required POST parameters are "currencyPair",
	# "rate", and "amount". If successful, the method will return the order number.
	# Inputs:
	# currencyPair  The curreny pair
	# rate		  price the order is selling at
	# amount		Amount of coins to sell
	# Outputs:
	# orderNumber   The order number
	def sell( self, currencyPair, rate, amount ):
		params = {
			"currencyPair": currencyPair,
			"rate": rate,
			"amount": amount
			}
		return self.api_query( 'sell', params )

	def cancel( self, currencyPair, orderNumber ):
		"""
		Cancels an order you have for a given currency pair with an order number.
		Returns True if the cancellation was a success, otherwise False.
		"""
		params = {
			"currencyPair": currencyPair,
			"orderNumber": orderNumber
			}
		res = self.api_query( 'cancelOrder', params )
		return bool( int( res['success'] ) )

	def withdraw( self, currency, amount, address ):
		"""
		Withdraw the specified currency in the specified amount to the
		specified address.
		WITHDRAWLS MUST BE ENABLED FOR THIS API KEY FOR THIS TO WORK.
		Returns data in the following format:
		{ "response": "Withdrew 2398 NXT." }
		"""
		params = {
			"currency": currency,
			"amount": amount,
			"address": address
			}
		return self.api_query( 'withdraw', params )
