# Pyloniex

## A Python 3 API for [Poloniex](https://poloniex.com/)

###### (Thanks to [oipminer](http://pastebin.com/8fBVpjaj) for the starting code)

Welcome! This is intended to be a user-friendly wrapper to the Poloniex API, implementing everything the standard API does and then some for convenience. It is implemented in Python 3.5.

Documentation is embedded within the code.
